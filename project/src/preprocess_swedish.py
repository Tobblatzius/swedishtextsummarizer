#%%
import pandas as pd
import json
import os
import spacy
from spacy.lang.sv import Swedish
from sklearn.model_selection import train_test_split
import re
#%%
# convert json files to tsv.
files = ['/Users/Rikard/Desktop/data/' + path for path in os.listdir('/Users/Rikard/Desktop/data')]
with open('../raw_data/news_articles.txt','w+') as doc:

    doc.write('title\ttext\n')
    #for _ in range(10000):
    for file in files:
     #   file = next(files)
        data = json.load(open(file))
        title = re.sub(r'[\s]+', ' ', data['title'])
        title = re.sub(r'^\s+|\s+$', '', title)
        text = re.sub(r'[\s]+', ' ', data['text'])
        text = re.sub(r'^\s+|\s+$', '', text)
        doc.write(title+'\t'+text+'\n')
#%%
data = pd.read_csv('../raw_data/news_articles.txt', sep='\t', error_bad_lines=False)
#data = data[['title', 'text']]
data = data[data.text.apply(lambda x: type(x)==str) & data.title.apply(lambda x: type(x)==str)]
data = data[data.text.apply(lambda x: x.count('.') > 3)]

#%%
for i in range(20):
    print(i, len(data[data.title.apply(lambda x: len(x.split()) > i)]))


#%%
data = data[data.title.apply(lambda x: len(x.split()) > 10)]

data.head()
#%%
train, temp = train_test_split(data)
test, validation = train_test_split(temp, train_size=0.5)
# %%
nlp = Swedish()
nlp.add_pipe(nlp.create_pipe('sentencizer'))
datasets = ['train', 'test', 'valid']

for name, data in zip(datasets, [train, test, validation]):
    with open(f'../json_data_clean10/news_{name}.json', 'w+') as outfile:
        outfile.write('[\n')
        for idx, (_, (title, text)) in enumerate(data.iterrows()):
            if idx != 0: outfile.write(',\n')
            text_toks = nlp(text)
            title_toks = nlp(title)
            title = [[token.text for token in sent] for sent in title_toks.sents]
            text = [[token.text for token in sent] for sent in text_toks.sents]
            json.dump({'src': text, 'tgt':title}, outfile, indent=2,  ensure_ascii=False)
            # if idx > 1000: break
        outfile.write('\n]')


# %%
