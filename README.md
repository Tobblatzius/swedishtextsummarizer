# SwedishTextSummarizer

Using a Swedish pretrained BERT model from https://arxiv.org/abs/2007.01658 in a text summarizer according to https://arxiv.org/abs/1908.08345. Trained and evaluated on Swedish news articles from https://webhose.io/free-datasets/swedish-news-articles  

Look at this notebook for running examples: https://colab.research.google.com/drive/1ZOnuRQ0BJK1qhSESlvqTWT0_pMoUzYRR?usp=sharing



